[//Design Patterns](../home)

# Behavioral Design Patterns

Design patterns that identify common communication patterns between objects and realize them, increasing flexibility in carrying out communication. Used to manage algorithms, relationships, and responsibilities between objects.

## [Chain of Responsibility](../ChainOfResponsibility)
Command objects are handled or passed on to other objects by processing objects. Gives more than one object an opportunity to handle a request by linking receiving objects together.

## [Command](../Command)
Command objects encapsulate an action and its parameters. Encapsulates a request allowing it to be treated as an object. This allows the request to be handled in traditionally object based relationships such as queuing and callbacks.

## [Interpreter](../Interpreter)
Allows for including language elements into a program. Defines a representation for a grammar as well as a mechanism to understand and act upon the grammar.

## [Iterator](../Iterator)
Sequentially access the elements of a collection. Allows for access to the elements of an aggregate object without allowing access to its underlying representation.

## [Mediator](../Mediator)
Define simplified communication between classes. Allows loose coupling by encapsulating the way disparate sets of objects interact and communicate with each other. Allows for the actions of each object set to vary independently of one another.

## [Memento](../Memento)
Capture and restore an object's internal state. Allows for capturing and externalizing an object’s internal state so that it can be restored later, all without violating encapsulation.

## [Null Object](../NullObject) 
Designed to act as a default value of an object. The intent of a Null Object is to encapsulate the absence of an object by providing a substitutable alternative that offers suitable default do nothing behavior.

## [Observer](../Observer)
A way of notifying change to a number of classes. Lets one or more objects be notified of state changes in other objects within the system

## [State](../State)
Alter an object's behavior when its state changes. Ties object circumstances to its behavior, allowing the object to behave in different ways based upon its internal state.

## [Strategy](../Strategy)
Encapsulates an algorithm inside a class. Defines a set of encapsulated algorithms that can be swapped to carry out a specific behavior.

## [Template Method](../TemplateMethod)
Defer the exact steps of an algorithm to a subclass. Identifies the framework of an algorithm, allowing implementing classes to define the actual behavior.

## [Visitor](../Visitor)
Defines a new operation to a class without change. Allows for one or more operations to be applied to a set of objects at runtime, decoupling the operations from the object structure.
