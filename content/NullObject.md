[//Design Patterns](home)[/Behavioral Design Patterns](BehavioralDesignPatterns)

# Null Object

## Purpose
The intent of a Null Object is to encapsulate the absence of an object by providing a substitutable alternative that offers suitable default do nothing behavior.

![NullObject](/uploads/0a6bb90d6cb3f9c4a0e45d79d3d73bcf/NullObject.png)

## Applicable When
* an object requires a collaborator. The Null Object pattern does not introduce this collaboration--it makes use of a collaboration that already exists.
* some collaborator instances should do nothing.
* you want to abstract the handling of null away from the client.


