[//Design Patterns](home)[/Creational Design Patterns](CreationalDesignPatterns)

# Object Pool

## Purpose
Avoid expensive acquisition and release of resources by recycling objects that are no longer in use.


##  Applicable When

## Example
