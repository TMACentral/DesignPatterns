[//Design Patterns](../home)

# Structural Design Patterns

These design patterns are all about Class and Object composition. Structural class-creation patterns use inheritance to compose interfaces. Structural object-patterns define ways to compose objects to obtain new functionality. Used to form large object structures between many disparate objects.

## [Adapter](../Adapter)
Match interfaces of different classes. Permits classes with disparate interfaces to work together by creating a common object by which they may communicate and interact.
## [Bridge](../Bridge)
Separates an object’s interface from its implementation. Defines an abstract object structure independently of the implementation object structure in order to limit coupling.
## [Composite](../Composite)
A tree structure of simple and composite objects. Facilitates the creation of object hierarchies where each object can be treated independently or as a set of nested objects through the same interface.
## [Decorator](../Decorator)
Add responsibilities to objects dynamically. Allows for the dynamic wrapping of objects in order to modify their existing responsibilities and behaviors.
## [Facade](../Facade)
A single class that represents an entire subsystem. Supplies a single interface to a set of interfaces within a system.
## [Flyweight](../Flyweight)
A fine-grained instance used for efficient sharing. Facilitates the reuse of many  fine-grained objects, making the utilization of large numbers of objects more efficient.
## [Proxy](../Proxy)
An object representing another object. Allows for object level access control by acting as a pass-through entity or a placeholder object.

