'# Design Patterns
In software engineering, a design pattern is a general repeatable solution to a commonly occurring problem in software design. A design pattern isn't a finished design that can be transformed directly into code. It is a description or template for how to solve a problem that can be used in many different situations.

_"Each pattern describes a problem that occurs over and over again in an environment, and then describes the core of the solution to that problem, in such a way that you can use the solution a million times over, without ever doing it the same way twice - Christopher Alexander"_


Design patterns can speed up the development process by providing tested, proven development paradigms. Effective software design requires considering issues that may not become visible until later in the implementation. Reusing design patterns help to prevent subtle issues that can cause major problems and improves code readability for coders and architects familiar with the patterns.

Often, people only understand how to apply certain software design techniques to certain problems. These techniques are difficult to apply to a broader range of problems. Design patterns provide general solutions, documented in a format that doesn't require specifics tied to a particular problem.

In addition, patterns allow developers to communicate using well-known, well-understood names for software interactions. Common design patterns can be improved over time, making them more robust than ad-hoc designs.

## [Behavioral Design Patterns](../BehavioralDesignPatterns)
Design patterns that identify common communication patterns between objects and realize them, increasing flexibility in carrying out communication. Used to manage algorithms, relationships, and responsibilities between objects.

## [Creational Design Patterns](../CreationalDesignPatterns)
Design patterns that deal with class instantiation, and can be divided into class-creation patterns and object creation patterns. Used to construct objects such that they can be decoupled from their implementing system.

## [Structural Design Patterns](../StructuralDesignPatterns)
These design patterns are all about Class and Object composition. Structural class-creation patterns use inheritance to compose interfaces. Structural object-patterns define ways to compose objects to obtain new functionality. Used to form large object structures between many disparate objects.

